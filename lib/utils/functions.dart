import 'package:flutter/material.dart';

void showSnackBar (BuildContext context, String message){
    SnackBar snackBar = new SnackBar(
        content: Text(message), 
        duration: Duration(milliseconds: 2000)
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
}