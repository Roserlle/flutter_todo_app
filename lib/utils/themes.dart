import 'package:flutter/material.dart';

var themeApp = ThemeData(
    primaryColor: Color.fromRGBO(255, 212, 71, 1),
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
            primary: Color.fromRGBO(255, 212, 71, 1),
            onPrimary: Colors.black
        )
    )
);