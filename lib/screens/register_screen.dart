import 'dart:async';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

import '/utils/api.dart';
import '/utils/functions.dart';

class RegisterScreen extends StatefulWidget {
    @override
    RegisterScreenState createState() => RegisterScreenState();
}

class RegisterScreenState extends State<RegisterScreen> {
    Future<bool>?  _futureUser;
    final _formKey = GlobalKey<FormState>();
    final _tffEmailController = TextEditingController();
    final _tffPasswordController = TextEditingController();
    final _tffConfirmPasswordController = TextEditingController();

    void register(BuildContext context){
        setState(() {
            _futureUser = API().register(
                email: _tffEmailController.text, 
                password: _tffPasswordController.text
            ).catchError((error){
                showSnackBar(context, error.message);
            });
        });
    }  

    @override
    Widget build(BuildContext context) {
        Widget tffEmail = TextFormField(
            decoration: InputDecoration(labelText: 'Email'),
            keyboardType: TextInputType.emailAddress,
            controller : _tffEmailController,
            validator: (email) {
                if(email == null || email.isEmpty){
                    return 'The email must be provided';
                } else if(EmailValidator.validate(email) == false ){
                    return 'A valid email must be provided';
                } 
                return null;
            }
        );

        Widget tffPassword = TextFormField(
            decoration: InputDecoration(labelText: 'Password'),
            obscureText: true,
            controller : _tffPasswordController,
            validator: (password) {
                return (password != null && password.isNotEmpty) ? null : 'The password must be provided';
            }
        );

        Widget tffConfirmPassword = TextFormField(
            decoration: InputDecoration(labelText: 'Confirm Password'),
            obscureText: true,
            controller : _tffConfirmPasswordController,
            validator: (passwordConfirmation) {
                if(passwordConfirmation == null || passwordConfirmation.isEmpty){
                    return 'The password confirmation must be provided';
                } else if(_tffPasswordController.text != passwordConfirmation){
                    return 'The password confirmation does not match with the password above';
                } 
                return null;
            }
        );

        Widget btnRegister = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 8.0),
            child: ElevatedButton(
                child: Text('Register'),
                onPressed: () {
                    if(_formKey.currentState!.validate()){
                        register(context);
                    } else {
                        showSnackBar(context, 'Update registration fields to pass all validations.');
                    }
                    //pushReplacementMethod para wala na back button
                    //Navigator.pushReplacementNamed(context, '/task-list');
                }
            )
        );

        Widget btnGoToLogin = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 8),
            child: ElevatedButton(
                child: Text('Go to Login'),
                onPressed: () {
                    //No back button
                    Navigator.pop(context);
                }
            )
        );

        Widget formRegister = SingleChildScrollView(
            child: Form(
                key: _formKey,
                child: Column(
                    children: [
                        tffEmail,
                        tffPassword,
                        tffConfirmPassword,
                        btnRegister,
                        btnGoToLogin
                    ]
                )
            )
        );

        Widget registerView = FutureBuilder(
            future: _futureUser,
            builder: (context, snapshot){
                if(_futureUser == null){
                    return formRegister;
                } else if(snapshot.hasData && snapshot.data == true){
                    Timer(Duration(seconds: 3), (){
                        //pushReplacementNamed LoginScreen <- LoginScreen
                        //pushNamedAndRemoveUnti = LoginScree -> RegisterScreen then mawawala lahat to -> LoginScreen 
                        Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
                    });
                    return Column(
                        children: [
                            Text('Registration successful, You will be redirected shortly back to the login page.'),
                        ]
                    );
                }else {
                    return Center(
                        child: CircularProgressIndicator()
                    );
                }
            }
        );
        
        return Scaffold(
            appBar: AppBar(title: Text('Todo Account Registration')),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: registerView
            )
        );
    }
}