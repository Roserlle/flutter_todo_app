import 'dart:io';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '/models/task.dart';
import '/providers/user_provider.dart';
import '/utils/api.dart';
import '/utils/functions.dart';

class TaskDetailScreen extends StatefulWidget {
    final Task _task;

    TaskDetailScreen(this._task);

    @override
    TaskDetailScreenState createState() => TaskDetailScreenState();
}

class TaskDetailScreenState extends State<TaskDetailScreen> {
    Future<Task>? _futureImageUpload;

    @override
    Widget build(BuildContext context) {
        final String? accessToken = Provider.of<UserProvider>(context).accessToken;

        Widget btnUpload = Container(
            width: double.infinity,
            padding: EdgeInsets.only(top: 16.0),
            child: ElevatedButton(
                child: Text('Upload Image'),
                onPressed: () async {
                    var selectedImage = await ImagePicker().pickImage(source: ImageSource.gallery);

                    setState(() {
                        if (selectedImage != null){
                            var image;
                            if (kIsWeb) {
                                image = Image.network(selectedImage.path);
                            } else {
                                image = Image.file(File(selectedImage.path));
                            }

                            _futureImageUpload = API(accessToken).addTaskImage(
                                id: widget._task.id, 
                                filePath: image.path
                            ).catchError((error) {
                                showSnackBar(context, error.message);
                            });
                        }
                    });
                }
            )
        );

        Widget imageView = FutureBuilder(
            future: _futureImageUpload,
            builder: (context, snapshot) {
                return Container();
            }
        );

        return Scaffold(
            appBar: AppBar(title: Text('Task Detail')),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: SingleChildScrollView(
                    child: Column(
                        children: [
                            Text(widget._task.description),
                            btnUpload,
                            imageView
                        ]
                    )
                )
            )
        );
    }
}