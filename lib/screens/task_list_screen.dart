import 'package:flutter/material.dart';
import 'package:flutter_todo_app/utils/api.dart';
import 'package:flutter_todo_app/widgets/add_task_dialog.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';


import '/models/task.dart';
import '/providers/user_provider.dart';
import '/utils/api.dart';
import '/utils/functions.dart';
import '/widgets/task_item_tile.dart';


class TaskListScreen extends StatefulWidget {
    @override
    TaskListScreenState createState() => TaskListScreenState();
}

class TaskListScreenState extends State<TaskListScreen> {
    Future<List<Task>>? _futureTasks;

    void showAddTaskDialog(BuildContext context) {
        final String? accessToken = Provider.of<UserProvider>(context, listen: false).accessToken;
        showDialog(
            context: context,
            builder: (BuildContext context) => AddTaskDialog()
        ).then((value) {
            setState(() {
                _futureTasks = API(accessToken).getTasks().catchError((error){
            showSnackBar(context, error.message);
                });
            });
        });
    }

    Widget showTasks(List? tasks) {
        var cltTasks = tasks!.map((task) => TaskItemTile(task)).toList();
        return ListView(
            children: cltTasks
        );
    }

   @override
    void initState() {
        super.initState();

        WidgetsBinding.instance!.addPostFrameCallback((timestamp) {
            final String? accessToken = context.read<UserProvider>().accessToken;

            setState(() {
                _futureTasks = API(accessToken).getTasks().catchError((error) {
                    showSnackBar(context, error.message);
                });
            });
        });
    }

    @override
    Widget build(BuildContext context) {
        Widget taskListView = FutureBuilder(
            future: _futureTasks,
            builder: (context, snapshot){
                if(snapshot.hasData) {
                    return showTasks(snapshot.data as List);
                }
                return Center(
                    child: CircularProgressIndicator()
                );
            }
        );
        return Scaffold(
            appBar: AppBar(title: Text('Todo List')),
            drawer: Drawer(
                child: ListView(
                    children: [
                        ListTile(
                            hoverColor: Color.fromRGBO(255, 212, 71, 1),
                            title: Text('Logout'),
                            onTap: () async {
                                Provider.of<UserProvider>(context, listen: false).setAccessToken(null);
                                Provider.of<UserProvider>(context, listen: false).setUserId(null);

                                SharedPreferences prefs = await SharedPreferences.getInstance();

                                prefs.remove('accessToken');
                                prefs.remove('userId');

                                Navigator.pushNamedAndRemoveUntil(context, '/', (Route<dynamic> route) => false);
                            }
                        )
                    ]
                )
            ),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: taskListView
            ),
            floatingActionButton: FloatingActionButton(
                child: Icon(Icons.add),
                backgroundColor: Color.fromRGBO(255, 212, 71, 1),
                foregroundColor: Colors.black,
                onPressed: () {
                    showAddTaskDialog(context);
                }
            )
        );
    }
}