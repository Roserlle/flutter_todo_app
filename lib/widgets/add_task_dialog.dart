import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/providers/user_provider.dart';
import '/utils/api.dart';
import '/utils/functions.dart';

class AddTaskDialog extends StatefulWidget {
    @override
    AddTaskDialogState createState() => AddTaskDialogState();
}

class AddTaskDialogState extends State<AddTaskDialog> {
    final tffDescriptionController = TextEditingController();

    void addTask(BuildContext context) {
        final String? accessToken = Provider.of<UserProvider>(context, listen: false).accessToken;

        API(accessToken).addTask(
            description: tffDescriptionController.text
        ).catchError((error) {
            showSnackBar(context, error.message);
        });
    }

    @override
    Widget build(BuildContext context) {
        return AlertDialog(
            title: Text('Add New Task'),
            content: Container(
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                        TextFormField(
                            decoration: InputDecoration(labelText: 'Task Description'),
                            keyboardType: TextInputType.text,
                            controller: tffDescriptionController
                        )
                    ]
                )
            ),
            actions: [
                ElevatedButton(
                    child: Text('Add'),
                    onPressed: () {
                        addTask(context);
                        Navigator.of(context).pop();
                    }
                ),
                ElevatedButton(
                    child: Text('Cancel'),
                    onPressed: () {
                        Navigator.of(context).pop();
                    }
                ),
            ],
        );
    }
}