import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';
part 'user.g.dart';

@freezed
class User with _$User {
    const factory User({
        int? id,
        String? email,
        String? accessToken
    }) = _User;

    factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}

//Model-View-Controller
//View (Flutter) <=> Model (Class)
//Controller (API) <=> Model (Database)

//https://github.com/dart-lang/pub/issues/2460#issuecomment-821064484